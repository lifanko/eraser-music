## 橡皮音乐 Pro

EraserMusic-Pro implements a high performance JS audio player, very small & efficient.

### Why pro?
 + Responsive Design
 + Smooth Animation
 + Flexbox Driver
 + High Efficient Music Player: Eraser-Player
 + New Backend Design
 
### Environment
 + PHP 7.4+
 + Redis 6.0+
 
### Installation
 + Download Code
 + composer install (Make sure Composer has been installed)
 + Complete (Please make sure your server has [PHP] & [Redis] Environment)

### Extra
If you're using this code, please SET A FRIEND-LINK in your website.

### Screenshot
![Demo](https://gitee.com/lifanko/eraser-music/raw/master/screenshot.jpg)

## 运行不起来怎么办？

请添加QQ群：366265211（橡皮音乐交流群）

![qq群](qq.png)